define(["require", "exports", "angular"], function (require, exports, angular) {
    var application = angular.module("application", ["ngRoute", "ngResource", "ui.bootstrap"]);
    return application;
});
//# sourceMappingURL=Application.js.map