﻿import application = require("application");
import SaveUserController = require("Controllers/User/SaveUserController");
import UserListController = require("Controllers/User/UserListController");
import SaveTodoController = require("Controllers/Todo/SaveTodoController");

//console.log("AddUserController1", AddUserController, "UserListController1", UserListController);

application.config(function ($routeProvider) {
    console.log("routeprovider :",$routeProvider);
    $routeProvider.
        when('/saveuser', { controller: SaveUserController, templateUrl: 'App/Views/User/Add.html' }).
        when('/saveuser/:id', { controller: SaveUserController, templateUrl: 'App/Views/User/Add.html' }).
        when('/userlist', { controller: UserListController, templateUrl: 'App/Views/User/List.html' }).
        when('/savetodo', { controller: SaveTodoController, templateUrl:'App/Views/Todo/Add.html' }).
        otherwise({ redirectTo: '/userlist' });
});