﻿import application = require("application");
import jquery = require("jquery");
export var load: boolean; 

class UserService implements IUserService {
    httpServiceUrl: string;
    httpService: ng.IHttpService;
     
    constructor($http: ng.IHttpService) {
        this.httpService = $http;      
        this.httpServiceUrl = '/api';
    }

    GetAll(callback) {
        this.httpService.get(this.httpServiceUrl + '/user').success(function (users) {
            callback(users);
        })
        .error(function (error) {
            console.log('Unable to load users data: ', error.message);
       }); 
    }

    Get(id: number, callback) {   
        this.httpService.get(this.httpServiceUrl + '/user/'+ id).success(function (user) {
            callback(user);
        })
        .error(function (error) {
            console.log('Unable to load user data: ', error.message);
        }); 
    }

    Save(user: IUser, callback) {
        this.httpService.post(this.httpServiceUrl + '/user', user).success(function (user) {
            callback(user);
        })
        .error(function (error) {
            console.log('Unable to save user data: ', error.message);
        });
    }

    Update(user: IUser, callback) {
        this.httpService.put(this.httpServiceUrl + '/user/' + user.id, user).success(function (data) {
            callback();
        })
        .error(function (error) {
            console.log('Unable to update user data: ', error.message);
        });
    }

    Delete(id: number, callback) {
        this.httpService.delete(this.httpServiceUrl + '/user/' + id).success(function (data) {
            callback();
        })
        .error(function (error) {
            console.log('Unable to delete user data: ', error.message);
        }); 
    }
}

application.service('UserService', UserService);