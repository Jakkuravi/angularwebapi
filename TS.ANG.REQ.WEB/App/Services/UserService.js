define(["require", "exports", "application"], function (require, exports, application) {
    var UserService = (function () {
        function UserService($http) {
            this.httpService = $http;
            this.httpServiceUrl = '/api';
        }
        UserService.prototype.GetAll = function (callback) {
            this.httpService.get(this.httpServiceUrl + '/user').success(function (users) {
                callback(users);
            })
                .error(function (error) {
                console.log('Unable to load users data: ', error.message);
            });
        };
        UserService.prototype.Get = function (id, callback) {
            this.httpService.get(this.httpServiceUrl + '/user/' + id).success(function (user) {
                callback(user);
            })
                .error(function (error) {
                console.log('Unable to load user data: ', error.message);
            });
        };
        UserService.prototype.Save = function (user, callback) {
            this.httpService.post(this.httpServiceUrl + '/user', user).success(function (user) {
                callback(user);
            })
                .error(function (error) {
                console.log('Unable to save user data: ', error.message);
            });
        };
        UserService.prototype.Update = function (user, callback) {
            this.httpService.put(this.httpServiceUrl + '/user/' + user.id, user).success(function (data) {
                callback();
            })
                .error(function (error) {
                console.log('Unable to update user data: ', error.message);
            });
        };
        UserService.prototype.Delete = function (id, callback) {
            this.httpService.delete(this.httpServiceUrl + '/user/' + id).success(function (data) {
                callback();
            })
                .error(function (error) {
                console.log('Unable to delete user data: ', error.message);
            });
        };
        return UserService;
    })();
    application.service('UserService', UserService);
});
//# sourceMappingURL=UserService.js.map