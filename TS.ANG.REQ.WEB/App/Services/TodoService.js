define(["require", "exports", "application"], function (require, exports, application) {
    var TodoService = (function () {
        function TodoService($http) {
            this.httpService = $http;
            this.httpServiceUrl = '/api';
        }
        TodoService.prototype.GetAll = function (callback) {
            this.httpService.get(this.httpServiceUrl + '/todo').success(function (todos) {
                callback(todos);
            })
                .error(function (error) {
                console.log('Unable to load todos data: ', error.message);
            });
        };
        TodoService.prototype.Get = function (id, callback) {
            this.httpService.get(this.httpServiceUrl + '/todo/' + id).success(function (todo) {
                callback(todo);
            })
                .error(function (error) {
                console.log('Unable to load todo data: ', error.message);
            });
        };
        TodoService.prototype.Save = function (todo, callback) {
            this.httpService.post(this.httpServiceUrl + '/todo', todo).success(function (todo) {
                callback(todo);
            })
                .error(function (error) {
                console.log('Unable to save todos data: ', error.message);
            });
        };
        TodoService.prototype.Update = function (todo, callback) {
            this.httpService.put(this.httpServiceUrl + '/todo/' + todo.id, todo).success(function (data) {
                callback();
            })
                .error(function (error) {
                console.log('Unable to update todo data: ', error.message);
            });
        };
        TodoService.prototype.Delete = function (id, callback) {
            this.httpService.delete(this.httpServiceUrl + '/todo/' + id).success(function (data) {
                callback();
            })
                .error(function (error) {
                console.log('Unable to delete todo data: ', error.message);
            });
        };
        return TodoService;
    })();
    application.service('TodoService', TodoService);
});
//# sourceMappingURL=TodoService.js.map