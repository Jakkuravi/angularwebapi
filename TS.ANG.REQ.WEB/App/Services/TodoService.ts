﻿import application = require("application");
import jquery = require("jquery");
export var load: boolean;

class TodoService implements ITodoService {
    httpServiceUrl: string;
    httpService: ng.IHttpService;

    constructor($http: ng.IHttpService) {
        this.httpService = $http;
        this.httpServiceUrl = '/api';
    }

    GetAll(callback) {
        this.httpService.get(this.httpServiceUrl + '/todo').success(function (todos) {
            callback(todos);
        })
            .error(function (error) {
                console.log('Unable to load todos data: ', error.message);
            });
    }

    Get(id: number, callback) {
        this.httpService.get(this.httpServiceUrl + '/todo/' + id).success(function (todo) {
            callback(todo);
        })
            .error(function (error) {
                console.log('Unable to load todo data: ', error.message);
            });
    }

    Save(todo: ITodo, callback) {
        this.httpService.post(this.httpServiceUrl + '/todo', todo).success(function (todo) {
            callback(todo);
        })
            .error(function (error) {
                console.log('Unable to save todos data: ', error.message);
            });
    }

    Update(todo: ITodo, callback) {
        this.httpService.put(this.httpServiceUrl + '/todo/' + todo.id, todo).success(function (data) {
            callback();
        })
            .error(function (error) {
                console.log('Unable to update todo data: ', error.message);
            });
    }

    Delete(id: number, callback) {
        this.httpService.delete(this.httpServiceUrl + '/todo/' + id).success(function (data) {
            callback();
        })
            .error(function (error) {
                console.log('Unable to delete todo data: ', error.message);
            });
    }
}

application.service('TodoService', TodoService);