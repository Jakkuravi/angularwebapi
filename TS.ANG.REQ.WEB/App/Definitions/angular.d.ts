﻿// TypeScript declarations useful for importing angular modules
declare module 'angular' {
    var angular: ng.IAngularStatic;
    export = angular;
}

declare module 'angular-route' {
}

declare module 'angular-resource' {
}

declare module 'angular-ui-bootstrap' {
}