﻿interface ITodo {
    id: number;
    name: string;
    status:boolean;
    completeDate: Date;
}

interface ISaveTodoScope extends ITodo {
    Todo: ITodo;
    Reset(): void;
    Submit(): void;
    TodoList(): void;
}

interface ISaveTodoRouteParams {
    id: number;
}

interface ITodoListScope {
    Todos: Array<ITodo>;
    GetAll(): void;
    Edit(id: number): void;
    Delete(id: number): void;
    AddTodo(): void;
}

interface ITodoService {
    GetAll(callback): void;
    Get(id: number, callback): void;
    Save(todo: ITodo, callback): void;
    Update(todo: ITodo, callback): void;
    Delete(id: number, callback): void;
}