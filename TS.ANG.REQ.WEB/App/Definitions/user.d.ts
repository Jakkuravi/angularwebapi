﻿interface IUser {
    id: number;
    name: string;
    address: string;
    email: string;
}

interface ISaveUserScope extends IUser {   
    User: IUser; 
    Reset(): void;
    Submit(): void;
    UserList(): void;
}

interface ISaveUserRouteParams {
   id: number;
}

interface IUserListScope {
    Users: Array<IUser>;
    GetAll(): void;
    Edit(id: number): void;
    Delete(id: number): void;
    AddUser(): void;
}

interface IUserService {       
    GetAll(callback): void;
    Get(id: number, callback): void;
    Save(user: IUser, callback): void;
    Update(user: IUser, callback): void;
    Delete(id: number, callback): void;
}