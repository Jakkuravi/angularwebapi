﻿import application = require("application");
import UseService = require('Services/UserService');
UseService.load = true;

export = SaveUserController;
class SaveUserController {
    routeParams: ISaveUserRouteParams;
    scope: ISaveUserScope;   
    userService: IUserService
    location: ng.ILocationService

    constructor(public $scope: ISaveUserScope, $routeParams: ISaveUserRouteParams, UserService: IUserService, $location: ng.ILocationService) {
        var self = this;
        self.routeParams = $routeParams;
        self.scope = $scope;   
        self.userService = UserService;
        self.location = $location;  

        $scope.Reset = function () {
            self.SetUser();
        };

        $scope.Submit = function () {
            self.SaveUser();
        };

        $scope.UserList = function () {
            self.RedirectToUserList();
        };

        self.SetUser();
    }

    RedirectToUserList() {
        this.location.path('/userlist');
    }

    SetUser() {  
        var self = this;

        if (this.routeParams.id) {
            this.userService.Get(this.routeParams.id, function (user) {
                self.scope.User = user; 
            });
        } else {
            this.scope.User = { id: 0, name: "", address: "", email: "" }; 
        }
    };

    SaveUser() {
        var self = this;   
        if (this.routeParams.id) {
            this.userService.Update(this.$scope.User, function () {
                self.RedirectToUserList();
            });
        } else {
            this.userService.Save(this.$scope.User, function (user) {
                self.RedirectToUserList();
            });
        }
    };
}