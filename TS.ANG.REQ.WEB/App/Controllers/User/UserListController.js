define(["require", "exports", 'Services/UserService'], function (require, exports, UseService) {
    UseService.load = true;
    var UserListController = (function () {
        function UserListController($scope, UserService, $location) {
            this.$scope = $scope;
            this.scope = $scope;
            this.userService = UserService;
            var self = this;
            $scope.AddUser = function () {
                $location.path('/saveuser');
            };
            $scope.Edit = function (id) {
                $location.path('/saveuser/' + id);
            };
            $scope.Delete = function (id) {
                self.Delete(id);
            };
            this.GetAll();
        }
        UserListController.prototype.GetAll = function () {
            var self = this;
            this.userService.GetAll(function (users) {
                self.scope.Users = users;
            });
        };
        UserListController.prototype.Delete = function (id) {
            var self = this;
            this.userService.Delete(id, function () {
                self.GetAll();
            });
        };
        return UserListController;
    })();
    return UserListController;
});
//# sourceMappingURL=UserListController.js.map