﻿import application = require("application");
import UseService = require('Services/UserService');
UseService.load = true;

export = UserListController;
class UserListController {
    scope: IUserListScope;   
    userService: IUserService

    constructor(public $scope: IUserListScope, UserService: IUserService, $location: ng.ILocationService) {    
        this.scope = $scope;                     
        this.userService = UserService;            
        var self = this;

        $scope.AddUser = function () {
            $location.path('/saveuser');
        };

        $scope.Edit = function (id: number) {
            $location.path('/saveuser/'+ id);
        };

        $scope.Delete = function (id: number) {
            self.Delete(id);
        };

        this.GetAll();
    }

    GetAll() {
        var self = this;

        this.userService.GetAll(function (users) {
           self.scope.Users = users;            
        });    
    }

    Delete(id: number) {
        var self = this;
        this.userService.Delete(id, function () {
            self.GetAll();
        });
    }
}