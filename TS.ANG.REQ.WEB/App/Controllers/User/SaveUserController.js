define(["require", "exports", 'Services/UserService'], function (require, exports, UseService) {
    UseService.load = true;
    var SaveUserController = (function () {
        function SaveUserController($scope, $routeParams, UserService, $location) {
            this.$scope = $scope;
            var self = this;
            self.routeParams = $routeParams;
            self.scope = $scope;
            self.userService = UserService;
            self.location = $location;
            $scope.Reset = function () {
                self.SetUser();
            };
            $scope.Submit = function () {
                self.SaveUser();
            };
            $scope.UserList = function () {
                self.RedirectToUserList();
            };
            self.SetUser();
        }
        SaveUserController.prototype.RedirectToUserList = function () {
            this.location.path('/userlist');
        };
        SaveUserController.prototype.SetUser = function () {
            var self = this;
            if (this.routeParams.id) {
                this.userService.Get(this.routeParams.id, function (user) {
                    self.scope.User = user;
                });
            }
            else {
                this.scope.User = { id: 0, name: "", address: "", email: "" };
            }
        };
        ;
        SaveUserController.prototype.SaveUser = function () {
            var self = this;
            if (this.routeParams.id) {
                this.userService.Update(this.$scope.User, function () {
                    self.RedirectToUserList();
                });
            }
            else {
                this.userService.Save(this.$scope.User, function (user) {
                    self.RedirectToUserList();
                });
            }
        };
        ;
        return SaveUserController;
    })();
    return SaveUserController;
});
//# sourceMappingURL=SaveUserController.js.map