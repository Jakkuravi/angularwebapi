﻿import application = require("application");
import ToService = require('Services/TodoService');
ToService.load = true;

export = SaveTodoController;
class SaveTodoController {
    routeParams: ISaveTodoRouteParams;
    scope: ISaveTodoScope;
    todoService: ITodoService
    location: ng.ILocationService

    constructor(public $scope: ISaveTodoScope, $routeParams: ISaveTodoRouteParams, TodoService: ITodoService, $location: ng.ILocationService) {
        var self = this;
        self.routeParams = $routeParams;
        self.scope = $scope;
        self.todoService = TodoService;
        self.location = $location;

        $scope.Reset = function () {
            self.SetTodo();
        };

        $scope.Submit = function () {
            self.SaveTodo();
        };

        $scope.TodoList = function () {
            self.RedirectToTodoList();
        };

        self.SetTodo();
    }

    RedirectToTodoList() {
        this.location.path('/todolist');
    }

    SetTodo() {
        var self = this;

        if (this.routeParams.id) {
            this.todoService.Get(this.routeParams.id, function (todo) {
                self.scope.Todo = todo;
            });
        } else {
            this.scope.Todo = { id: 0, name: "", status: false, completeDate: null };
        }
    };

    SaveTodo() {
        var self = this;
        if (this.routeParams.id) {
            this.todoService.Update(this.$scope.Todo, function () {
                self.RedirectToTodoList();
            });
        } else {
            this.todoService.Save(this.$scope.Todo, function (todo) {
                self.RedirectToTodoList();
            });
        }
    };
}