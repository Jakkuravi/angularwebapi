define(["require", "exports", 'Services/TodoService'], function (require, exports, ToService) {
    ToService.load = true;
    var SaveTodoController = (function () {
        function SaveTodoController($scope, $routeParams, TodoService, $location) {
            this.$scope = $scope;
            var self = this;
            self.routeParams = $routeParams;
            self.scope = $scope;
            self.todoService = TodoService;
            self.location = $location;
            $scope.Reset = function () {
                self.SetTodo();
            };
            $scope.Submit = function () {
                self.SaveTodo();
            };
            $scope.TodoList = function () {
                self.RedirectToTodoList();
            };
            self.SetTodo();
        }
        SaveTodoController.prototype.RedirectToTodoList = function () {
            this.location.path('/todolist');
        };
        SaveTodoController.prototype.SetTodo = function () {
            var self = this;
            if (this.routeParams.id) {
                this.todoService.Get(this.routeParams.id, function (todo) {
                    self.scope.Todo = todo;
                });
            }
            else {
                this.scope.Todo = { id: 0, name: "", status: false, completeDate: null };
            }
        };
        ;
        SaveTodoController.prototype.SaveTodo = function () {
            var self = this;
            if (this.routeParams.id) {
                this.todoService.Update(this.$scope.Todo, function () {
                    self.RedirectToTodoList();
                });
            }
            else {
                this.todoService.Save(this.$scope.Todo, function (todo) {
                    self.RedirectToTodoList();
                });
            }
        };
        ;
        return SaveTodoController;
    })();
    return SaveTodoController;
});
//# sourceMappingURL=SaveTodoController.js.map