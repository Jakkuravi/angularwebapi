﻿var require: any;

require.config({
    urlArgs: "v=24022016.1103",
    baseUrl: 'App',
    paths: {
        'jquery': '../libs/js/jquery/2.2.0/jquery.min',
        'angular': '../libs/js/angularjs/1.5.0/angular.min',
        'angular-route': '../libs/js/angularjs/1.5.0/angular-route.min',
        'angular-resource': '../libs/js/angularjs/1.5.0/angular-resource.min',
        'angular-ui-bootstrap': '../libs/css/angular-ui/1.1.2/ui-bootstrap-tpls.min'
    },
    shim: {
        'jquery': { exports: 'jquery' },
        'angular': { exports: 'angular', dep: ['jquery'] },
        'angular-route': { exports: 'angular-route', deps: ['angular'] },
        'angular-resource': { exports: 'angular-resource', deps: ['angular'] },
        'angular-ui-bootstrap': { exports: 'angular-ui-bootstrap', deps: ['angular'] },
    },
});



require(['jquery', 'angular', 'angular-route', 'angular-resource', 'angular-ui-bootstrap', 'application', 'routes'], //'bootstrap', 
    function ($: JQueryStatic, angular: ng.IAngularStatic, angularRoute, angularResource, angularUiBootstrap, application, routes) {
        $(function () {
            angular.bootstrap(document, ['application']);
        });
    });