﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using TS.ANG.REQ.WEB.WebAPI.Models;

namespace TS.ANG.REQ.WEB.WebAPI.Controllers
{
    public class UserController : ApiController
    {             

        // GET api/<controller>
        public IEnumerable<User> Get()
        {
            return UserModel.Users;
        }

        // GET api/<controller>/5
        public IHttpActionResult Get(int id)
        {
            var user = UserModel.Users.FirstOrDefault((p) => p.id == id);
            if (user == null)
            {
                return NotFound();
            }
            return Ok(user);
        }

        // POST api/<controller>
        public IHttpActionResult Post(User user)
        {
            var maxId = UserModel.Users.Count > 0 ? UserModel.Users.Max(u => u.id) : 0;
            user.id = maxId + 1;

            UserModel.Users.Add(user);
            return Ok(user);
        }

        // PUT api/<controller>/5
        public IHttpActionResult Put(int id, User user)
        {
            for (int counter = 0; counter < UserModel.Users.Count; counter++)
            {
                if (UserModel.Users[counter].id == id)
                {
                    UserModel.Users[counter] = user;
                    return Ok();
                }
            }

            return NotFound();
        }

        // DELETE api/<controller>/5
        public IHttpActionResult Delete(int id)
        {
            for (int counter = 0; counter < UserModel.Users.Count; counter++)
            {
                if (UserModel.Users[counter].id == id)
                {
                    UserModel.Users.RemoveAt(counter);
                    return Ok();
                }
            }

            return NotFound();
        }
    }
}