﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using TS.ANG.REQ.WEB.WebAPI.Models;

namespace TS.ANG.REQ.WEB.WebAPI.Controllers
{
    public class TodoController : ApiController
    {
        // GET api/<controller>
        public IEnumerable<Todo> Get()
        {
            return TodoModel.Todos;
        }

        // GET api/<controller>/5
        public IHttpActionResult Get(int id)
        {
            var todo = TodoModel.Todos.FirstOrDefault((p) => p.id == id);
            if (todo == null)
            {
                return NotFound();
            }
            return Ok(todo);
        }

        // POST api/<controller>
        public IHttpActionResult Post(Todo todo)
        {
            var maxId = TodoModel.Todos.Count > 0 ? TodoModel.Todos.Max(u => u.id) : 0;
            todo.id = maxId + 1;

            TodoModel.Todos.Add(todo);
            return Ok(todo);
        }

        // PUT api/<controller>/5
        public IHttpActionResult Put(int id, Todo todo)
        {
            for (int counter = 0; counter < TodoModel.Todos.Count; counter++)
            {
                if (TodoModel.Todos[counter].id == id)
                {
                    TodoModel.Todos[counter] = todo;
                    return Ok();
                }
            }

            return NotFound();
        }

        // DELETE api/<controller>/5
        public IHttpActionResult Delete(int id)
        {
            for (int counter = 0; counter < TodoModel.Todos.Count; counter++)
            {
                if (TodoModel.Todos[counter].id == id)
                {
                    TodoModel.Todos.RemoveAt(counter);
                    return Ok();
                }
            }

            return NotFound();
        }
    }
}
