﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace TS.ANG.REQ.WEB.WebAPI.Models
{
    public class User
    {
        public int id { get; set; }
        public string name { get; set; }
        public string address { get; set; }
        public string email { get; set; }
    }
    
    public class UserModel
    {
        public static List<User> Users;

        static UserModel()
        {
            Users = new List<User>()
            {
               new User { id = 1, name = "A123", address = "A123-Adr", email = "A123@A123.com" },
               new User { id = 2, name = "B123", address = "B123-Adr", email = "B123@B123.com" }
            };            
        }
    }
}