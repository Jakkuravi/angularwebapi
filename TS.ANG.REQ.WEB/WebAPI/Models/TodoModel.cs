﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TS.ANG.REQ.WEB.WebAPI.Models
{
    public class Todo
    {
        public int id { get; set; }
        public string name { get; set; }
        public Boolean status { get; set; }
        public DateTime completeDate { get; set; }
    }

    public class TodoModel
    {
        public static List<Todo> Todos;

        static TodoModel()
        {
            Todos = new List<Todo>()
            {
               new Todo { id = 1, name = "A123", status = false, completeDate = DateTime.Now  },
               new Todo { id = 2, name = "B123", status = true, completeDate = DateTime.Now }
            };
        }
    }
}